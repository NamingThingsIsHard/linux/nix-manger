An attempt to write a package and option manager for nixos.

# Why the name

`nix-package-options-manager` just seemed a bit too long.

# How it works

We use ideas and code from [`nixos-search/flake-info`][flake-info].

For reading:

 - All possible options and packages are retrieved with `nix-env` and `nix eval` in JSON format
 - The current configuration is printed in JSON with [`builtins.toJSON`][builtins.toJSON]

For writing:

For now, the configuration is written as JSON and then imported with 
[`builtins.fromJSON`][builtins.fromJSON]


[builtins.fromJSON]: https://nixos.org/manual/nix/stable/expressions/builtins.html#builtins-fromJSON
[builtins.toJSON]: https://nixos.org/manual/nix/stable/expressions/builtins.html#builtins-toJSON
[flake-info]: https://github.com/NixOS/nixos-search/tree/main/flake-info

# TODO

 - [ ] Make JSON output contain the names of derivations not their paths
   (Will probably require some regex to parse the derivation string)

# Useful links

 - Where nixos evaluates configuration.nix
   [1](https://github.com/NixOS/nixpkgs/blob/master/nixos/default.nix)
   [2](https://github.com/NixOS/nixpkgs/blob/master/nixos/lib/eval-config.nix)
 - Where `nix eval` transforms stuff to JSON
   [1](https://github.com/NixOS/nix/blob/0e54fab0dd7b65c777847d6e80f1ca11233a15eb/src/nix/eval.cc#L119)
 - 
