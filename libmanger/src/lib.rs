/**
nix-manger: Graphical package and option manager for nixOS
Copyright (C) 2022  LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


use std::fs;

use anyhow::{anyhow, Result};
use lazy_regex::{regex_is_match, regex_replace};

pub use models::MangerContext;

use crate::io::{write_options, write_packages};

mod get_configuration_nix;
mod io;
mod models;

pub const MANGER_DIR_NAME: &str = "manger";

pub fn init_manger(context: &MangerContext) -> Result<()> {
    let manger_dir = context.manger_dir()?;
    let manger_dir_clone = manger_dir.clone();
    if !manger_dir.is_dir() {
        fs::create_dir(&manger_dir).map_err(|err| {
            anyhow!(
                "Couldn't create {}: {}",
                manger_dir_clone.to_str().unwrap_or("unknown dir"),
                err
            )
        })?;

        // Initialize empty selected options and packages
        write_options(vec![], &context)?;
        write_packages(vec![], &context)?;
    }

    // The file we need
    let manger_nix = context.configuration_dir()?.join("manger.nix");
    if !manger_nix.is_file() {
        fs::write(manger_nix.as_path(), include_str!("resources/manger.nix"))?
    }

    let configuration = read_configuration(&context)?;
    if !is_manger_imported(&configuration) {
        write_configuration(&context, &add_manger_to_imports(&configuration))?
    }

    Ok(())
}

/// Reads the configuration.nix from the location in the context
pub fn read_configuration(context: &MangerContext) -> Result<String> {
    Ok(fs::read_to_string(context.configuration_nix()?.as_path())?)
}

/// Checks if manger.nix is being imported into configuration.nix
pub fn is_manger_imported(configuration: &String) -> bool {
    // Find ./manger.nix in: imports = [ ... ]
    regex_is_match!(r#"imports\s*=\s*\[[^\]]+(\./manger\.nix)[^\]]*\]"#, configuration.as_str())
}

/// Add manger.nix to the imports in configuration.nix
pub fn add_manger_to_imports(configuration: &String) -> String {
    // TODO Add a check to make sure there's an `imports` attribute in the set
    let text = regex_replace!(
        r#"(imports\s*=\s*\[[^\]]+)\]"#,
        configuration.as_str(),
        |_, imports: &str| {
            let new_line = if imports.contains("\n") { "\n" } else { "" };
            format!("{}{}./manger.nix ]", imports, new_line)
        },
    );
    text.into_owned()
}

/// Reads the configuration.nix from the location in the context
pub fn write_configuration(context: &MangerContext, new_content: &String) -> Result<()> {
    let configuration_nix = context.configuration_nix()?;
    Ok(fs::write(configuration_nix.as_path(), new_content)?)
}
