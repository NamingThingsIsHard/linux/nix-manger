/**
nix-manger: Graphical package and option manager for nixOS
Copyright (C) 2022  LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

{
 pkgs ? (import <nixpkgs> {})
 , ...
}: with builtins; let

    # Read the options managed by nix-manger
    options = fromJSON (readFile ./manger/options.json);
    # Read the package names managed by nix-manger
    package_names = fromJSON (readFile ./manger/packages.json);
    packages = map (name: (getAttr name pkgs)) package_names;
in
    options // {
        environment.systemPackages = packages;
    }
