/**
nix-manger: Graphical package and option manager for nixOS
Copyright (C) 2022  LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::fs;
use flake_info::data::import::{NixOption};
use crate::models::MangerContext;

/**
Write all files to the manger and ensures that configuration.nix imports it
 */
pub fn write_manger(options: Vec<NixOption>, packages: Vec<String>, context: &MangerContext) -> anyhow::Result<()> {
    crate::init_manger(context)?;
    write_options(options, &context)?;
    write_packages(packages, &context)?;
    Ok(())
}

/// Writes the options to options.json in the manger directory
pub fn write_options(options: Vec<NixOption>, context: &MangerContext) -> anyhow::Result<()> {
    let options_path = context.options_path()?;
    fs::write(options_path, serde_json::to_string(&options)?)?;
    Ok(())
}

/// Read options in the manger directory
pub fn read_options(context: &MangerContext) -> anyhow::Result<Vec<NixOption>> {
    let options_path = context.options_path()?;
    let options = serde_json::from_str(fs::read_to_string(options_path)?.as_str())?;
    Ok(options)
}

/// Writes packages to packages.json in the manger directory
pub fn write_packages(packages: Vec<String>, context: &MangerContext) -> anyhow::Result<()> {
    let packages_path = context.packages_path()?;
    fs::write(packages_path, serde_json::to_string(&packages)?)?;
    Ok(())
}

/// Reads packages.json from manger directory
pub fn read_packages(context: &MangerContext) -> anyhow::Result<Vec<String>> {
    let packages_path = context.packages_path()?;
    let packages_val: Vec<String> = serde_json::from_str(fs::read_to_string(packages_path)?.as_str())?;
    Ok(packages_val)
}
