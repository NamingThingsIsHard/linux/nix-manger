/**
nix-manger: Graphical package and option manager for nixOS
Copyright (C) 2022  LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use anyhow::{Context, Result};
use command_run::{Command, LogTo};
use serde_json::Value;

pub fn get_configuration_nix() -> Result<Value> {
    let mut command = Command::new("nix");
    command.add_args(&[
        "eval",
        "--json",
        "--expr",
        include_str!("resources/config_to_json.nix"),
    ]);

    command.enable_capture();
    command.log_to = LogTo::Log;
    command.log_output_on_error = true;

    command
        .run()
        .with_context(|| {
            "Failed to gather information about the configuration"
        })
        .and_then(|o| {
            let output = &*o.stdout_string_lossy();
            Ok(serde_json::from_str(output)?)
        })
}
