/**
nix-manger: Graphical package and option manager for nixOS
Copyright (C) 2022  LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::path::{Path, PathBuf};
use anyhow::anyhow;

#[derive(Clone)]
pub struct MangerContext<'a> {
    configuration_dir: Option<&'a Path>,
    configuration_name: Option<String>,
}

impl Default for MangerContext<'_> {
    fn default() -> Self {
        MangerContext {
            configuration_dir: Some(Path::new("/etc/nixos/")),
            configuration_name: Some(String::from("configuration.nix")),
        }
    }
}

impl<'a> MangerContext<'_> {
    pub fn new(configuration_dir: Option<&'a Path>, configuration_name: Option<String>) -> MangerContext {
        MangerContext {
            configuration_dir,
            configuration_name,
        }
    }
    pub fn configuration_dir(&self) -> anyhow::Result<&Path> {
        self.configuration_dir.clone().ok_or(anyhow!("Configuration dir not passed"))
    }
    pub fn configuration_name(&self) -> anyhow::Result<String> {
        self.configuration_name.clone().ok_or(anyhow!("Configuration name not passed"))
    }
    pub fn configuration_nix(&self) -> anyhow::Result<PathBuf> {
        let dir = self.configuration_dir()?.clone();
        let name = self.configuration_name()?.clone();
        Ok(dir.join(name))
    }
    pub fn manger_dir(&self) -> anyhow::Result<PathBuf> {
        let dir = self.configuration_dir()?.clone();
        Ok(dir.join("manger"))
    }
    pub fn options_path(&self) -> anyhow::Result<PathBuf>{
        let manger_dir = self.manger_dir()?;
        Ok(manger_dir.join("options.json"))
    }
    pub fn packages_path(&self) -> anyhow::Result<PathBuf>{
        let manger_dir = self.manger_dir()?;
        Ok(manger_dir.join("packages.json"))
    }
}
