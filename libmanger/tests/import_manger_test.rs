/**
nix-manger: Graphical package and option manager for nixOS
Copyright (C) 2022  LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
use std::fs;

use temp_dir::TempDir;

use libmanger::{add_manger_to_imports, is_manger_imported, MangerContext, read_configuration};

fn test_add_manger_to_imports(configuration: &str) -> String {
    let new_configuration = add_manger_to_imports(&configuration.to_string());
    assert!(is_manger_imported(&new_configuration));
    new_configuration
}

#[test]
fn test_write_to_indented() {
    test_add_manger_to_imports(r"
    { config, pkgs, ...} :
    {
        imports = [
            ./hardware-configuration.nix
        ];
    }
    ");
}

#[test]
fn test_write_to_single_line() {
    let config_str = test_add_manger_to_imports(r"
    { config, pkgs, ...} :
    {
        imports = [ ./hardware-configuration.nix ];
    }
    ");

    // Ensure that the ./manger.nix line doesn't have a new-line in the [ ]
    let imports = config_str
        .split("\n")
        .find(|&line| line.contains("imports") && line.contains("./manger.nix"));
    assert!(imports.is_some())
}
