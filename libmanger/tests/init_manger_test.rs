/**
nix-manger: Graphical package and option manager for nixOS
Copyright (C) 2022  LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
use std::fs;

use temp_dir::TempDir;

use libmanger::{init_manger, is_manger_imported, MangerContext, read_configuration};

#[test]
fn test_import_manger() {
    let d = TempDir::new().unwrap();
    let context: MangerContext = MangerContext::new(
        Some(d.path()),
        Some("configuration.nix".to_string()),
    );
    // Write the initial file
    let config_path = context.configuration_nix().unwrap();
    fs::write(&config_path, r"
    { config, pkgs, ...} :
    {
        imports = [
            ./hardware-configuration.nix
        ];
    }
    ").unwrap();

    // Initialize nix-manger
    init_manger(&context).unwrap();

    // Checks
    assert!(is_manger_imported(&read_configuration(&context).unwrap()));
    assert!(context.manger_dir().unwrap().is_dir());
    assert!(context.configuration_dir().unwrap().join("manger.nix").is_file());
    assert!(context.manger_dir().unwrap().join("options.json").is_file());
    assert!(context.manger_dir().unwrap().join("packages.json").is_file());
}
