Feature: Import manger into the current configuration

  Scenario: Indented list of imports in configuration.nix with manger
    Given An indented list of imports containing manger
    """
    { config, pkgs, ...} :
    {
        imports = [
            ./hardware-configuration.nix
            ./manger.nix
        ];
    }
    """
    When configuration.nix is parsed
    Then it should detect that manger is being imported


  Scenario: Single line of imports in configuration.nix with manger
    Given A single list of imports containing manger
    """
    { config, pkgs, ...} :
    {
        imports = [ ./hardware-configuration.nix ./manger.nix ];
    }
    """
    When configuration.nix is parsed
    Then it should detect that manger is being imported


  Scenario: Indented list of imports in configuration.nix without manger
    Given An indented list of imports that don't contain manger
    """
    { config, pkgs, ...} :
    {
        imports = [
            ./hardware-configuration.nix
        ];
    }
    """
    When configuration.nix is parsed
    Then it should detect that manger is **not** being imported


  Scenario: Single line of imports in configuration.nix without
    Given A single list of imports that don't contain manger
    """
    { config, pkgs, ...} :
    {
        imports = [ ./hardware-configuration.nix ];
    }
    """
    When configuration.nix is parsed
    Then it should detect that manger is **not** being imported
