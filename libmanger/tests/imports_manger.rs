extern crate core;

/**
nix-manger: Graphical package and option manager for nixOS
Copyright (C) 2022  LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
use std::fs;
use std::time::Duration;

use cucumber::{given, then, when, World as _, writer};
use gherkin::Step;
use temp_dir::TempDir;
use tokio::time::sleep;

use libmanger::{is_manger_imported, MangerContext};

#[derive(cucumber::World, Debug, Default)]
struct World {
    configuration: Option<String>,
    imports: bool,
}

#[given(regex = r".+")] // Cucumber Expression
async fn configuration_is_given(w: &mut World, step: &Step) {
    w.configuration = Some(
        step.docstring.as_ref().expect("The configuration is required to be in the docstring").clone()
    )
}

#[when("configuration.nix is parsed")]
async fn check_imports(w: &mut World) {
    w.imports = is_manger_imported(w.configuration.as_ref().unwrap())
}

#[then("it should detect that manger is being imported")]
async fn is_importing(w: &mut World) {
    assert!(w.imports)
}

#[then("it should detect that manger is **not** being imported")]
async fn is_not_importing(w: &mut World) {
    assert!(!w.imports)
}

#[tokio::main]
async fn main() {
    World::cucumber()
        .with_writer(writer::Libtest::or_basic())
        .run("tests/features/imports_manger.feature")
        .await;
}
