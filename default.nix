let
  pkgs = import <nixpkgs> {};
  lib = pkgs.lib;
  stdenv = pkgs.stdenv;
in
stdenv.mkDerivation {
    name = "nix-manger"; # Probably put a more meaningful name here
    buildInputs = with pkgs;[
        gcc
        rustup
        pkg-config
        openssl
        openssl.dev
    ];

}
