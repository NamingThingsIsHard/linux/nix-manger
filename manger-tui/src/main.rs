/**
nix-manger: Graphical package and option manager for nixOS
Copyright (C) 2023  LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use cursive::Cursive;
use cursive::traits::*;

use crate::ui::build_ui;

mod ui;

// TODO follow tutorial https://github.com/gyscos/cursive/blob/main/doc/tutorial_3.md
fn main() {
    // TODO: Add params for the context manager

    // Creates the cursive root - required for every application.
    let mut siv = cursive::default();
    siv.add_global_callback('q', Cursive::quit);

    // Creates a dialog with a single "Quit" button
    siv.add_layer(build_ui().full_screen());

    // Starts the event loop.
    siv.run();
}
