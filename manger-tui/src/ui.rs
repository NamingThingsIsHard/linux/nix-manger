/**
nix-manger: Graphical package and option manager for nixOS
Copyright (C) 2022  LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use cursive::view::Nameable;
use cursive::traits::*;
use cursive::views::{LinearLayout, NamedView, TextView};
use cursive_tabs::TabPanel;
use cursive_tree_view::{Placement, TreeView};

pub fn build_ui() -> TabPanel {
    TabPanel::new()
        .with_tab(build_options_tab())
        .with_tab(build_packages_tab())
}

fn build_options_tab() -> NamedView<LinearLayout> {
    let mut tree = TreeView::new();

    tree.insert_item("root".to_string(), Placement::LastChild, 0);

    // TODO fill with options
    tree.insert_item("1".to_string(), Placement::LastChild, 0);
    tree.insert_item("2".to_string(), Placement::LastChild, 1);
    tree.insert_item("3".to_string(), Placement::LastChild, 2);

    let mut form_layout = LinearLayout::vertical();
    let mut layout = LinearLayout::horizontal()
        .child(tree)
        .child(form_layout);
    layout.with_name("Options")
}

fn build_packages_tab() -> NamedView<TextView> {
    // TODO add a tree view
    // TODO fill tree view with packages
    TextView::new("This is the packages view").with_name("Packages")
}
